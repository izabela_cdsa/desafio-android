package com.gitrepo.api;

import com.gitrepo.model.Application;
import com.gitrepo.model.RepoPull;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by izabela on 1/7/18.
 */

public interface RepoService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<Application> getRepo(@Query("page") int page);

    @GET("repos/{login}/{name}/pulls?state=all")
    Call<List<RepoPull>> getRepoPull(@Path("login") String login, @Path("name") String name);

}
