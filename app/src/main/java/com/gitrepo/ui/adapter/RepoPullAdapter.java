package com.gitrepo.ui.adapter;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.gitrepo.R;
import com.gitrepo.model.RepoPull;
import com.gitrepo.util.ScreenManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RepoPullAdapter extends RecyclerView.Adapter<RepoPullAdapter.RepoPullViewHolder> {

    private Context context;
    private List<RepoPull> repoPulls;

    String TAG = "REPOPULLADAPTER";

    public RepoPullAdapter(List<RepoPull> repoPulls, Context context) {
        this.context = context;
        this.repoPulls = repoPulls;
    }

    public RepoPullViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.repo_pull_item, parent, false);
        return new RepoPullViewHolder(view);
    }

    public void onBindViewHolder(final RepoPullViewHolder holder, int position) {
        final RepoPull item = repoPulls.get(position);

        holder.textViewDate.setText(String.valueOf(item.created_at));
        holder.textViewTitlePull.setText(String.valueOf(item.title));
        holder.textViewBodyPull.setText(String.valueOf(item.body));
        Picasso.with(context).load(item.user.avatar_url).into(holder.imageViewAccount);
        holder.textViewUserName.setText(String.valueOf(item.user.login));
        holder.textViewCompleteName.setText(String.valueOf(item.user.id));

        holder.cardViewRepoPull.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ScreenManager.goToRepoPull(context, item.html_url);
            }
        });
    }

    public int getItemCount() {
        return (repoPulls != null) ? repoPulls.size() : 0;
    }

    class RepoPullViewHolder extends RecyclerView.ViewHolder {

        public CardView cardViewRepoPull;
        public TextView textViewDate;
        public TextView textViewTitlePull;
        public TextView textViewBodyPull;
        public ImageView imageViewAccount;
        public TextView textViewUserName;
        public TextView textViewCompleteName;

        public RepoPullViewHolder(View v) {
            super(v);

            cardViewRepoPull = v.findViewById(R.id.cv_repo_pull);
            textViewDate = v.findViewById(R.id.tv_created_at);
            textViewTitlePull = v.findViewById(R.id.tv_title_pull);
            textViewBodyPull = v.findViewById(R.id.tv_body_pull);
            imageViewAccount = v.findViewById(R.id.iv_account);
            textViewUserName = v.findViewById(R.id.tv_username);
            textViewCompleteName = v.findViewById(R.id.tv_complete_name);

        }
    }
}



