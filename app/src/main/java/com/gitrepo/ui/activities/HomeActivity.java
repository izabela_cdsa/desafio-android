package com.gitrepo.ui.activities;

import android.content.DialogInterface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.gitrepo.R;
import com.gitrepo.api.HttpClient;
import com.gitrepo.model.Application;
import com.gitrepo.ui.adapter.RepoAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        int page = 1;

        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.rv_repo);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(false);

        final ImageView imageView = findViewById(R.id.backdrop);
        final Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("GitRepo");
        Glide.with(this).load(R.mipmap.octocat).fitCenter().into(imageView);

        HttpClient httpClient =  new HttpClient();
        httpClient.getRepo(new Callback<Application>() {

            public void onResponse(Call<Application> call, Response<Application> response) {
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful());
                recyclerView.setAdapter(new RepoAdapter(response.body(), HomeActivity.this));
            }

            public void onFailure(Call<Application> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                new AlertDialog.Builder(HomeActivity.this).setMessage("Servidor indisponível no momento, por favor tente mais tarde").setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }).show();
            }
        }, page);
    }
}

