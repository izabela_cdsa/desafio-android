package com.gitrepo.model;

import java.util.Date;
import java.util.List;

public class RepoPull {

        public String url;
        public int id;
        public String html_url;
        public String diff_url;
        public String patch_url;
        public String issue_url;
        public int number;
        public String state;
        public boolean locked;
        public String title;
        public User user;
        public String body;
        public Date created_at;
        public Date updated_at;
        public Object closed_at;
        public Object merged_at;
        public String merge_commit_sha;
        public Object assignee;
        public List<Object> assignees;
        public List<Object> requested_reviewers;
        public List<Object> requested_teams;
        public Object milestone;
        public String commits_url;
        public String review_comments_url;
        public String review_comment_url;
        public String comments_url;
        public String statuses_url;
        public Head head;
        public Base base;
        public Links _links;
        public String author_association;
    }

